# Invoicexpress API

Invoicexpress API intends to make a API available for other modules to use.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/invoicexpress_api).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/invoicexpress_api).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure the InvoicExpress API at (/admin/config/invoicexpress-api).


## Maintainers

Current maintainers:
- João mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)

This project has been sponsored by:
- [Javali ADSI](https://www.javali.pt/)


## Services

- InvoicExpress API integration has 4 services available:

   - invoicexpress_api.invoices

   - invoicexpress_api.estimates

   - invoicexpress_api.clients
 
   - invoicexpress_api.sequences


## Invoices examples

```php

// Creates the invoice service
$service = \Drupal::service('invoicexpress_api.invoices');

// Invoice array
$invoice = [
    'date' => date('d/m/Y'),
    'due_date' => date('d/m/Y'),
    'reference' => 'reference',
    'client' => [
      'name' => 'client_name',
      'code' => 'client_code',
      'email' => 'test@email.com',
      'address' => 'client_address',
      'city' => 'client_city',
      'postal_code' => 'client_postal_code',
      'country' => 'client_country',
      'fiscal_id' => 'client_fiscal_id',
      'phone' => 'client_phone',
    ],
    'items' => [
      [
        'name' => 'item_name',
        'description' => 'item_description',
        'unit_price' => 1,
        'quantity' => 1,
      ]
    ],
    'observations' => 'observations',
    'sequence_id' => 'sequence_id_from_sequences_service',
];

// Creates the invoice, returns the remote invoice information, or errors
$response = $service->createDocument('invoices', $invoice);

// Gets the remote invoice id from response
$invoice_id = $response['invoice']['id'];

// Gets a remote invoice
$remote_invoice = $service->getDocument('invoices', $invoice_id);

// Updates the invoice, adds an item, returns the remote invoice information, 
// or errors
$remote_invoice['invoice']['items'][] = [
    'name' => 'item_2_name',
    'description' => 'item_2_description',
    'unit_price' => 1,
    'quantity' => 1,
];
$response = $service->updateDocument('invoices', $invoice_id, $remote_invoice);

// Changes the state of an invoice
$service->changeDocumentState('invoices', $invoice_id, 'finalized');
$service->changeDocumentState('invoices', $invoice_id, 'settled');

// Sends an email with the invoice pdf
$service->sendEmail('invoices', $invoice_id, 'client@email.com'); 

// Gets the invoice pdf link
$link = $service->getDocumentPdf($invoice_id);

// Gets all invoice documents
$link = $service->getRelatedDocuments($invoice_id);

// Lists/searches invoices
$params = [
    'type' => 'invoices',
]
$results = $service->listAll($params);


CLIENTS EXAMPLES
----------------

// Creates the clients service
$service = \Drupal::service('invoicexpress_api.clients');

// Client array
$client = [
    'name' => 'client_name',
    'code' => 'client_code',
    'email' => 'client_email',
    'address' => 'client_address',
    'city' => 'client_city',
    'postal_code' => 'client_postal_code',
    'country' => 'client_country',
    'fiscal_id' => 'client_fiscal_id',
    'phone' => 'client_phone',
];

// Creates the client, returns the remote client information, or errors
$response = $service->createClient($client);

// Gets the remote client id from response
$client_id = $response['client']['id'];

// Gets a remote client
$remote_client = $service->getClient($client_id);

// Updates the client,returns the remote client information, or errors
$remote_client['client']['phone'] = 'xxx';
$response = $service->updateClient($client_id, $remote_client);

// Gets all the client invoices
$invoices = $service->listInvoices($client_id);

// Lists all the clients
$clients = $service->listAll();

// Search clients by name
$clients = $service->findByName('xxx');

// Search clients by code
$clients = $service->findByCode('xxx');

ESTIMATES EXAMPLES
------------------

// Creates the estimates service
$service = \Drupal::service('invoicexpress_api.estimates');

// Estimate array
$estimate = [
    'date' => date('d/m/Y'),
    'due_date' => date('d/m/Y'),
    'reference' => 'reference',
    'client' => [
      'name' => 'client_name',
      'code' => 'client_code',
      'email' => 'client_email',
      'address' => 'client_address',
      'city' => 'client_city',
      'postal_code' => 'client_postal_code',
      'country' => 'client_country',
      'fiscal_id' => 'client_fiscal_id',
      'phone' => 'client_phone',
    ],
    'items' => [
      [
        'name' => 'item_name',
        'description' => 'item_description',
        'unit_price' => '0.81',
        'quantity' => 1,
      ]
    ],
];

// Creates the estimate, returns the remote estimate information, or errors
$response = $service->createEstimate('quotes', $estimate);

// Gets the remote estimate id from response
$estimate_id = $response['quote']['id'];

// Gets a remote estimate
$remote_estimate = $service->getEstimate('quotes', $estimate_id);

// Updates the estimate, adds an item, returns the remote estimate information,
// or errors
$remote_estimate['quote']['items'][] = [
    'name' => 'item_2_name',
    'description' => 'item_2_description',
    'unit_price' => '0.81',
    'quantity' => 1,
];
$response = $service->updateEstimate('quotes', $estimate_id, $remote_estimate);

// Changes the state of an estimate
$service->changeEstimateState('quotes', $estimate_id, 'finalized');
$service->changeEstimateState('quotes', $estimate_id, 'accepted');

// Sends an email with the estimate pdf
$service->sendEmail('quotes', $estimate_id, 'client@email.com'); 

// Gets the estimate pdf link
$link = $service->getDocumentPdf($estimate_id);

// Gets all estimate documents
$link = $service->getRelatedDocuments($estimate_id);

// Lists/searches estimate
$params = [
    'type' => 'quotes',
]
$results = $service->listAll($params);


Sequences EXAMPLES
------------------

// Creates the sequences service
$service = \Drupal::service('invoicexpress_api.sequences');

// Lists sequences
$results = $service->listAll();
```
