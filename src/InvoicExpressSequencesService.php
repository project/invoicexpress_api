<?php

namespace Drupal\invoicexpress_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * The InvoicExpressSequencesService service.
 *
 * @package Drupa\invoicexpress_api
 */
class InvoicExpressSequencesService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The base endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * InvoicExpressSequencesService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Invoicexpress_api settings.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http Client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $http_client,
    LoggerChannelFactory $loggerFactory,
  ) {
    $this->configFactory = $configFactory->get('invoicexpress_api.settings');
    $this->httpClient = $http_client;
    $this->loggerFactory = $loggerFactory;
    $this->endpoint = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com';
  }

  /**
   * Returns all your sequences.
   *
   * See https://invoicexpress.com/api-v2/sequences/list-all-6.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function listAll() {
    $uri = $this->endpoint . '/sequences.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Sequences-listAll');
  }

}
