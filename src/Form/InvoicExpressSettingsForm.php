<?php

namespace Drupal\invoicexpress_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure invoicexpress_api settings for this site.
 */
class InvoicExpressSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'invoicexpress_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['invoicexpress_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('invoicexpress_api.settings');
    $form['account_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ACCOUNT_NAME'),
      '#default_value' => $config->get('account_name'),
      '#description' => $this->t("Enter the account name."),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API_KEY'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t("Enter the API key."),
      '#required' => TRUE,
    ];

    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email subject'),
      '#default_value' => $config->get('email_subject'),
      '#description' => $this->t("Enter the subject for the email sent when an invoice is generated ."),
      '#required' => TRUE,
    ];

    $form['email_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email body'),
      '#default_value' => $config->get('email_body'),
      '#description' => $this->t("Enter the body for the email sent when an invoice is generated ."),
      '#required' => TRUE,
    ];

    $form['errors'] = [
      '#type' => 'select',
      '#title' => $this->t('Error handling'),
      '#default_value' => $config->get('errors'),
      '#options' => [
        'none' => $this->t("None"),
        'log' => $this->t("Database logging"),
        'screen' => $this->t("Output in page"),
        'both' => $this->t("Both"),
      ],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('invoicexpress_api.settings')
      ->set('account_name', $form_state->getValue('account_name'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('email_subject', $form_state->getValue('email_subject'))
      ->set('email_body', $form_state->getValue('email_body'))
      ->set('errors', $form_state->getValue('errors'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
