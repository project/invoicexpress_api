<?php

namespace Drupal\invoicexpress_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * The InvoicExpressEstimatesService service.
 */
class InvoicExpressEstimatesService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The base endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * InvoicExpressEstimatesService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Invoicexpress_api settings.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http Client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $http_client,
    LoggerChannelFactory $loggerFactory,
  ) {
    $this->configFactory = $configFactory->get('invoicexpress_api.settings');
    $this->httpClient = $http_client;
    $this->loggerFactory = $loggerFactory;
    $this->endpoint = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com';
  }

  /**
   * Creates a new quote, proforma or fees_note.
   *
   * See https://invoicexpress.com/api-v2/estimates/create-1.
   *
   * @param string $document_type
   *   The type of document you want to create.
   *   For example: quotes, proformas or fees_notes.
   * @param array $estimate
   *   Invoice data to be created, ex:
   *   $estimate = [
   *   'date' => date('d/m/Y'),
   *   'due_date' => date('d/m/Y', strtotime('+10 days')),
   *   'reference' => 'reference',
   *   'observations' => 'observations',
   *   'client' => [
   *     'name' => 'Teste Javali',
   *     'code' => 'teste_javali',
   *     'email' => 'teste@javali.pt',
   *     'address' => 'address',
   *     'city' => 'city',
   *     'postal_code' => 'postal_code',
   *     'country' => 'Portugal',
   *     'fiscal_id' => '11111111',
   *     'phone' => 'phone',
   *     'fax' => 'fax',
   *   ],
   *   'items' => [
   *     [
   *       'name' => 'Item 1 title',
   *       'description' => 'Item 1 desc',
   *       'unit_price' => 1,
   *       'quantity' => 1,
   *     ]
   *   ],
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function createEstimate(string $document_type, array $estimate) {
    $uri = $this->endpoint . '/' . $document_type . '.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      $document_type => $estimate,
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

  /**
   * Returns a specific estimate.
   *
   * See https://invoicexpress.com/api-v2/estimates/get-1.
   *
   * @param string $document_type
   *   The type of estimate you want to get.
   *   For example: quotes, proformas or fees_notes.
   * @param int $document_id
   *   The requested estimate id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function getEstimate($document_type, $document_id) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

  /**
   * Updates a quote, proforma or fees_note.
   *
   * See https://invoicexpress.com/api-v2/estimates/update-1.
   *
   * @param string $document_type
   *   The type of document you want to create.
   *   For example: quotes, proformas or fees_notes.
   * @param int $document_id
   *   The requested estimate id.
   * @param array $estimate
   *   Invoice data to be created, ex:
   *   $estimate = [
   *   'date' => date('d/m/Y'),
   *   'due_date' => date('d/m/Y', strtotime('+10 days')),
   *   'reference' => 'reference',
   *   'observations' => 'observations',
   *   'client' => [
   *     'name' => 'Teste Javali',
   *     'code' => 'teste_javali',
   *     'email' => 'teste@javali.pt',
   *     'address' => 'address',
   *     'city' => 'city',
   *     'postal_code' => 'postal_code',
   *     'country' => 'Portugal',
   *     'fiscal_id' => '11111111',
   *     'phone' => 'phone',
   *     'fax' => 'fax',
   *   ],
   *   'items' => [
   *     [
   *       'name' => 'Item 1 title',
   *       'description' => 'Item 1 desc',
   *       'unit_price' => 1,
   *       'quantity' => 1,
   *     ]
   *   ],
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function updateEstimate($document_type, $document_id, array $estimate) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      $document_type => $estimate,
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

  /**
   * Changes the state of a quote, proforma or fees_note.
   *
   * See https://invoicexpress.com/api-v2/estimates/change-state-1.
   *
   * @param string $document_type
   *   The type of document you wish to change.
   * @param int $document_id
   *   ID of the document to change state.
   * @param string $document_state
   *   State which the quote will be changed to.
   *   Options are: finalized, deleted, canceled, accepted, refused.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function changeEstimateState(string $document_type, int $document_id, string $document_state) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '/change-state.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      $document_type => [
        'state' => $document_state,
      ],
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

  /**
   * Returns all your estimates (quotes, proformas and fees notes).
   *
   * You can filter your estimates by passing parameters in the query string.
   *
   * See https://invoicexpress.com/api-v2/estimates/list-all-1.
   *
   * @param array $filter
   *   Array of params to pass to filter the results, ex:
   *   text (fulltext search), type, status, date[from], date[to], etc.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function listAll(array $filter = []) {
    $uri = $this->endpoint . '/estimates.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'body' => json_encode($filter),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

  /**
   * Returns the url of the PDF for the specified document.
   *
   * This is an asynchronous operation, the file may not be ready immediately.
   *
   * See https://invoicexpress.com/api-v2/estimates/generate-pdf-1.
   *
   * @param int $document_id
   *   The document to convert to PDF.
   *
   * @return string
   *   The pdf link.
   */
  public function getDocumentPdf(int $document_id) {
    $uri = $this->endpoint . '/api/pdf/' . $document_id . '.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    $response = invoicexpress_api_invoicexpress_handle_response($request);

    return (!empty($response['output']['pdfUrl'])) ? $response['output']['pdfUrl'] : FALSE;
  }

  /**
   * Sends a estimate by email.
   *
   * See https://invoicexpress.com/api-v2/estimates/send-by-email-1.
   *
   * @param string $document_type
   *   The type of document you wish to send by email.
   * @param int $document_id
   *   ID of the document to send by email.
   * @param string $email
   *   Document will be sent to this email address. Must be a valid email.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function sendEmail(string $document_type, int $document_id, $email) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '/email-document.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      "message" => [
        "client" => [
          'email' => $email,
          'save' => '0',
        ],
        "subject" => $this->configFactory->get('email_subject'),
        "body" => $this->configFactory->get('email_body'),
      ],
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request);
  }

}
