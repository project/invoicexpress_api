<?php

namespace Drupal\invoicexpress_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * The InvoicExpressClientsService service.
 */
class InvoicExpressClientsService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The base endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * InvoicExpressClientsService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Invoicexpress_api settings.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http Client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $http_client,
    LoggerChannelFactory $loggerFactory,
  ) {
    $this->configFactory = $configFactory->get('invoicexpress_api.settings');
    $this->httpClient = $http_client;
    $this->loggerFactory = $loggerFactory;
    $this->endpoint = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com';
  }

  /**
   * Returns a list of all your clients.
   *
   * See https://invoicexpress.com/api-v2/clients/list-all-4.
   *
   * @param int $page
   *   You can ask for a specific page of clients. Defaults to 1.
   * @param int $per_page
   *   You can specify how many results you want to fetch.
   *   Defaults to 10 or value defined in account settings (10, 20 or 30).
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function listAll(int $page = 1, int $per_page = 10) {
    $uri = $this->endpoint . '/clients.json?page=' . $page . '&per_page=' . $per_page . '&api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-listAll');
  }

  /**
   * Returns a specific client.
   *
   * See https://invoicexpress.com/api-v2/clients/get-4.
   *
   * @param int $client_id
   *   The ID of the client you want to get.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function getClient(int $client_id) {
    $uri = $this->endpoint . '/clients/' . $client_id . '.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-getClient');
  }

  /**
   * Returns a specific client.
   *
   * See https://invoicexpress.com/api-v2/clients/get-4.
   *
   * @param array $client
   *   Client data to be created, ex:
   *   $client = [
   *   'name' => 'Teste Javali',
   *   'code' => 'teste_javali',
   *   'email' => 'teste@javali.pt',
   *   'address' => 'address',
   *   'city' => 'city',
   *   'postal_code' => 'postal_code',
   *   'country' => 'Portugal',
   *   'fiscal_id' => '11111111',
   *   'phone' => 'phone',
   *   'fax' => 'fax',
   *   'observations' => 'observations',
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function createClient(array $client) {
    $uri = $this->endpoint . '/clients.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'client' => $client,
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-createClient');
  }

  /**
   * Updates a client.
   *
   * See https://invoicexpress.com/api-v2/clients/update-4.
   *
   * @param int $client_id
   *   The ID of the client you want to get.
   * @param array $client
   *   Client data to be created, ex:
   *   $client = [
   *   'name' => 'Teste Javali',
   *   'code' => 'teste_javali',
   *   'email' => 'teste@javali.pt',
   *   'address' => 'address',
   *   'city' => 'city',
   *   'postal_code' => 'postal_code',
   *   'country' => 'Portugal',
   *   'fiscal_id' => '11111111',
   *   'phone' => 'phone',
   *   'fax' => 'fax',
   *   'observations' => 'observations',
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function updateClient($client_id, array $client) {
    $uri = $this->endpoint . '/clients/' . $client_id . '.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'client' => $client,
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-updateClient');
  }

  /**
   * Returns a specific client.
   *
   * See https://invoicexpress.com/api-v2/clients/find-by-name.
   *
   * @param string $client_name
   *   The client name you want to search. Partial search is not supported.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function findByName(string $client_name) {
    $uri = $this->endpoint . '/clients/find-by-name.json?client_name=' . $client_name . '&api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-findByName');
  }

  /**
   * Returns a specific client.
   *
   * See https://invoicexpress.com/api-v2/clients/find-by-name.
   *
   * @param string $client_code
   *   The client code you want to search.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function findByCode(string $client_code) {
    $uri = $this->endpoint . '/clients/find-by-code.json?client_code=' . $client_code . '&api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-findByCode');
  }

  /**
   * This method allows you to obtain the invoices for a specific client.
   *
   * You can filter the results by status, type and if it's archived or not.
   *
   * https://invoicexpress.com/api-v2/clients/list-invoices.
   *
   * @param string $client_id
   *   The ID of the client you want to get.
   * @param array $filter
   *   Filters to apply on the list, ex:
   *   $filter = [
   *   "status" => 'final',
   *   "by_type" =>"Invoice",
   *   "archived" => "non_archived"
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function listInvoices(string $client_id, array $filter = []) {
    $uri = $this->endpoint . '/clients/' . $client_id . '/invoices.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'filter' => $filter,
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'headers' => [
          'body' => json_encode($json_data),
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Clients-listInvoices');
  }

}
