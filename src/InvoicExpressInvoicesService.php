<?php

namespace Drupal\invoicexpress_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * The InvoicExpressInvoicesService service.
 */
class InvoicExpressInvoicesService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The base endpoint.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * InvoicExpressInvoicesService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Invoicexpress_api settings.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http Client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $http_client,
    LoggerChannelFactory $loggerFactory,
  ) {
    $this->configFactory = $configFactory->get('invoicexpress_api.settings');
    $this->httpClient = $http_client;
    $this->loggerFactory = $loggerFactory;
    $this->endpoint = 'https://' . $this->configFactory->get('account_name') . '.app.invoicexpress.com';
  }

  /**
   * Creates a new invoice:.
   *
   * Ex: simplified_invoice, invoice_receipt, credit_note or debit_note.
   *
   * See https://invoicexpress.com/api-v2/invoices/create
   *
   * @param string $document_type
   *   The type of invoice you want to get.
   * @param array $invoice
   *   Invoice data to be created, ex:
   *   $invoice = [
   *   'date' => date('d/m/Y'),
   *   'due_date' => date('d/m/Y', strtotime('+10 days')),
   *   'reference' => 'reference',
   *   'client' => [
   *     'name' => 'client_name',
   *     'code' => 'client_code',
   *     'email' => 'test@email.com',
   *     'address' => 'client_address',
   *     'city' => 'client_city',
   *     'postal_code' => 'client_postal_code',
   *     'country' => 'client_country',
   *     'fiscal_id' => 'valid_client_fiscal_id',
   *     'phone' => 'client_phone',
   *     'fax' => 'client_fax',
   *   ],
   *   'items' => [
   *     [
   *       'name' => 'item_name',
   *       'description' => 'item_description',
   *       'unit_price' => '0.81',
   *       'quantity' => 1,
   *     ]
   *   ],
   *   'observations' => 'observations',
   *   'sequence_id' => 'sequence_id_from_sequences_service',
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function createDocument($document_type, array $invoice) {
    $uri = $this->endpoint . '/' . $document_type . '.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'invoice' => $invoice,
    ];

    try {
      $request = $this->httpClient->request('POST', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-createDocument');
  }

  /**
   * Updates an invoice.
   *
   * See https://invoicexpress.com/api-v2/invoices/update.
   *
   * @param string $document_type
   *   The type of invoice you want to get.
   * @param int $document_id
   *   The requested invoice id.
   * @param array $invoice
   *   Invoice data to be created, ex:
   *   $invoice = [
   *   'date' => date('d/m/Y'),
   *   'due_date' => date('d/m/Y', strtotime('+10 days')),
   *   'reference' => 'reference',
   *   'client' => [
   *     'name' => 'Teste Javali',
   *     'code' => 'teste_javali',
   *     'email' => 'teste@javali.pt',
   *     'address' => 'address',
   *     'city' => 'city',
   *     'postal_code' => 'postal_code',
   *     'country' => 'Portugal',
   *     'fiscal_id' => '11111111',
   *     'phone' => 'phone',
   *     'fax' => 'fax',
   *   ],
   *   'items' => [
   *     [
   *       'name' => 'Item 1 title',
   *       'description' => 'Item 1 desc',
   *       'unit_price' => 1,
   *       'quantity' => 1,
   *     ]
   *   ],
   *   'observations' => 'observations',
   *   'sequence_id' => 'sequence_id_from_sequences_service',
   *   ];.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function updateDocument($document_type, $document_id, array $invoice) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '.json?document-type=' . $document_type . 's&document-id=' . $document_id . '&api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'invoice' => $invoice,
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data, JSON_UNESCAPED_SLASHES),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-updateDocument');
  }

  /**
   * Returns a specific invoice.
   *
   * See https://invoicexpress.com/api-v2/invoices/get.
   *
   * @param string $document_type
   *   The type of invoice you want to get.
   * @param int $document_id
   *   The requested invoice id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function getDocument($document_type, $document_id) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-getDocument');
  }

  /**
   * Returns all the documents.
   *
   * See https://invoicexpress.com/api-v2/invoices/related-documents.
   *
   * @param int $document_id
   *   The requested invoice id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function getRelatedDocuments($document_id) {
    $uri = $this->endpoint . '/document/' . $document_id . '/related_documents.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-getRelatedDocuments');
  }

  /**
   * Returns all your invoices.
   *
   * You can filter your invoices by passing parameters in the query string.
   *
   * See https://invoicexpress.com/api-v2/invoices/list-all.
   *
   * @param array $params
   *   Array of params to pass to filter the results, ex:
   *   text (fulltext search), type, status, date[from], date[to], etc.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function listAll(array $params = []) {
    $uri = $this->endpoint . '/invoices.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = $this->httpClient->request('GET', $uri, [
        'body' => json_encode($params),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-listAll');
  }

  /**
   * Changes the state of an invoice.
   *
   * See https://invoicexpress.com/api-v2/invoices/change-state.
   *
   * @param string $document_type
   *   The type of document you wish to change state.
   *   For example: invoice, invoice_receipt, simplified_invoice,
   *   vat_moss_invoice, credit_note or debit_note.
   * @param int $document_id
   *   ID of the document to change state.
   * @param string $document_state
   *   State which the invoice will be changed to.
   *   Options are: finalized, deleted, second_copy, canceled,
   *   settled, unsettled.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function changeDocumentState(string $document_type, int $document_id, string $document_state) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '/change-state.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      'invoice' => [
        'state' => $document_state,
      ],
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-changeDocumentState');
  }

  /**
   * Sends a document by email.
   *
   * See https://invoicexpress.com/api-v2/invoices/send-by-email.
   *
   * @param string $document_type
   *   The type of document you wish to send by email.
   *   For example: invoice, invoice_receipt, simplified_invoice,
   *   vat_moss_invoice, credit_note or debit_note.
   * @param int $document_id
   *   ID of the document to send by email.
   * @param string $email
   *   Document will be sent to this email address.
   *   Must be a valid email address: foo@bar.com.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Decoded json of the body of the response.
   */
  public function sendEmail(string $document_type, int $document_id, $email) {
    $uri = $this->endpoint . '/' . $document_type . '/' . $document_id . '/email-document.json?api_key=' . $this->configFactory->get('api_key');

    $json_data = [
      "message" => [
        "client" => [
          'email' => $email,
          'save' => '0',
        ],
        "subject" => $this->configFactory->get('email_subject'),
        "body" => $this->configFactory->get('email_body'),
      ],
    ];

    try {
      $request = $this->httpClient->request('PUT', $uri, [
        'body' => json_encode($json_data),
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    return invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-sendEmail');
  }

  /**
   * Returns the url of the PDF for the specified document.
   *
   * This is an asynchronous operation, the file may not be ready immediately.
   *
   * See https://invoicexpress.com/api-v2/invoices/generate-pdf.
   *
   * @param int $document_id
   *   The document to convert to PDF.
   *
   * @return string
   *   The link to the pdf.
   */
  public function getDocumentPdf(int $document_id) {
    $uri = $this->endpoint . '/api/pdf/' . $document_id . '.json?api_key=' . $this->configFactory->get('api_key');

    try {
      $request = this->httpClient->request('GET', $uri, [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'http_errors' => FALSE,
      ]);
    }
    catch (GuzzleException $exception) {
      $this->loggerFactory->get('invoidexpress_api')->error($exception->getMessage());
      return FALSE;
    }

    $response = invoicexpress_api_invoicexpress_handle_response($request, 'Invoices-getDocumentPdf');

    return (!empty($response['output']['pdfUrl'])) ? $response['output']['pdfUrl'] : FALSE;
  }

}
